/***************************
* Author: Maruf Ahmed
* https://gitlab.com/maruf.ahmed
* **************************/

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <iomanip>
#include <vector>
#include <stack>
#include <bits/stdc++.h>
#include <algorithm>
#include <string>
#include <sstream>
#include<conio.h>

using namespace std;

int const N = 200;
vector<int> adj[N];
vector<int> edge_weight[N];
int  node_weight[N];
int  node_pred[N];
bool node_final[N];


void dijkstra (int s)
{
     int a, b, min_n, min_val;
     node_weight[s]  = 0;
     node_pred[s]    = s;

     for (int i = 0; i < N ; i++)
     {
        min_n = -1;
        min_val = INT_MAX;
        for (int j = 0; j < N; j++)
            if ( node_final[j] == false &&  node_weight[j] < min_val )
            {
                min_val = node_weight[j];
                min_n   = j;
            }

        if (min_n != -1)
        {
            node_final[min_n] = true;
            a = min_n;
        } else
            return;

        for (int j = 0; j < adj[a].size(); j++)
            if ( node_final[ adj[a][j] ] == false)
                if (node_weight[a] + edge_weight[a][j]  < node_weight[ adj[a][j] ] )
                {
                    node_weight[ adj[a][j] ] = node_weight[a] + edge_weight[a][j];
                    node_pred  [ adj[a][j] ] = a;
                }

     }
}

int main()
{
    ifstream file("dijkstraData.txt");
    string line, term, s;
    int a, b, w;

    for (int i = 0; i < N ; i++)
    {
        node_weight[i] = INT_MAX;
        node_pred[i]   = -1;
        node_final[i]  = false;
    }

    while ( getline (file, line))
    {
        istringstream iss(line);
        iss >> term;
        a = stoi (term);
        a--;
        while(iss >> term)
        {
           istringstream iss2(term);
           getline( iss2, s, ',' );
           b = stoi (s);
           b--;
           adj[a].push_back(b);
           getline( iss2, s, ',' );
           w = stoi (s);
           edge_weight[a].push_back(w);
        }
    }

    /******** START: display initial *******************
    for (int i = 0; i < N ; i++)
    {
        cout<<endl<<i<<":" <<endl;
        for (int j = 0; j < adj[i].size(); j++)
        {
            cout<< adj[i][j]<<":";
            cout<< edge_weight[i][j]<<"   ";
        }

    }
    /******** END: display initial *******************/
    int source = 0;
    dijkstra (source);

    /******** START: display Final *******************
    for (int i = 0; i < N ; i++)
        cout<<i+1<<":"<<node_weight[i]<<" Pred:"<<node_pred[i] << "  Final:"<<node_final[i]<<endl;
    /******** END: display Final *******************/

    ofstream outfile;
    outfile.open ("Result.txt");
    for (int i = 0; i < N ; i++)
        outfile<<"Node: "<<setw(3)<<i+1<<", Dist: "<<setw(4)<<node_weight[i]<<endl;
    outfile.close();

    int lookup[] = {7,37,59,82,99,115,133,165,188,197};
    for (int i = 0; i < N ; i++)
        for (int j = 0; j < (sizeof(lookup)/sizeof(*lookup))  ; j++)
            if (i+1 == lookup[j])
                cout<<"Node "<<i+1<<" : "<<node_weight[i]<<endl;

    exit(EXIT_SUCCESS);
}
