/***************************
* Author: Maruf Ahmed
* https://gitlab.com/maruf.ahmed
* **************************/

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <iomanip>
#include <vector>
#include <stack>
#include <bits/stdc++.h>
#include <algorithm>
#include<conio.h>

using namespace std;

//const long N = 875714;
const long N = 8;
long  M;


struct vertex {
   vector <long> adj;
   vector <long> adj_reverse;
};

vertex vertices [N];
bool   visited[N];
stack  <long> node_stack;

long   component_size[N];
vector <long> component_set[N];
long   no_of_component;

void dfs_forward (long a)
{
    long b;
    visited [a] = true;
    for (long i = 0; i < vertices [a].adj.size() ; i++  )
    {
        b = vertices[a].adj[i];
        if (visited[b] == false)
            dfs_forward(b);
    }
    node_stack.push(a);
}


void dfs_reverse (long a)
{
    long b;

    component_set[no_of_component].push_back(a);
    component_size[no_of_component]++;

    visited [a] = true;
    for (long i = 0; i < vertices [a].adj_reverse.size() ; i++  )
    {
        b = vertices[a].adj_reverse[i];
        if (visited[b] == false)
            dfs_reverse(b);
    }
}


int main()
{
    FILE *fp;
    size_t  len = 255;
    char    buffer[len];
    long    a, b;
    ssize_t read;
    long    i;

    fp = fopen ("SCC-2.txt", "r");
    if (fp == NULL)
    {
        printf("File open error \n");
        exit (EXIT_FAILURE);
    }

    M = 0;
    while (  fgets (buffer, len, fp)   != NULL)
    {
        sscanf( buffer, "%ld %ld", &a, &b );
        //***************************************/
        //a--; b--;
        cout<< setw(6) <<M<<": "<< setw(6) << a <<" "<< setw(6)<< b << endl;

        vertices[a].adj.push_back(b);
        vertices[b].adj_reverse.push_back(a);
        M++;
    }
    cout<<"N: "<<N<<endl;
    cout<<"M: "<<M<<endl;

    fclose(fp);

    for (long i = 0; i < N; i ++)
        visited[i] = false;

    for (long i = 0; i < N; i ++)
    {
        if (visited [i] == false){
            dfs_forward (i);
        }
    }
    cout<<endl;
    no_of_component = 0;
    for (long i = 0; i < N; i ++)
        visited[i] = false;

    while (node_stack.empty() == false)
    {
         a = node_stack.top();
         node_stack.pop();

         if (visited[a]==false)
         {
             dfs_reverse(a);
             no_of_component++;
         }

    }
    cout<<"Total components: "<<setw(3)<<no_of_component<<endl<<endl;
    cout<<"Show up to five largest NCC: "<<endl;
    cout<<"------------------------"<<endl;


    sort( component_size ,  component_size + N );
    for (long i = N-1; i > N-6; i --)
        if (component_size[i]>0 && i >= 0 )
            cout<<setw(6)<<component_size[i]<<"\n";

    exit(EXIT_SUCCESS);
}
