/***************************
* Author: Maruf Ahmed
* https://gitlab.com/maruf.ahmed
* **************************/

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <iomanip>
#include <vector>
#include <stack>
#include <bits/stdc++.h>
#include <algorithm>
#include <string>
#include <sstream>
#include<conio.h>

using namespace std;
int count_i;
long long a, b;
unordered_set < long long  > U_SET;

bool Two_sum (long long t)
{
    for (const long long &number : U_SET){
        a = number;
        b = t - number;
        //cout << a << "  "<<b<<endl;
        if (U_SET.find(b) == U_SET.end()) {
            continue;
        }
        else
        {
            cout << "Found:"<<t<<" ["<< a << ", "<<b<<" ]"<<endl;
            return true;
        }
    }
    //cout << "Key doesn't exist\n";
    return false;
}


bool check_entry (long long a, long long b)
{
    if ( U_SET.find(a) == U_SET.end() ||  U_SET.find(b) == U_SET.end() )
    {
        cout <<"Not Valid Entries"<<endl;
        return false;
    }
    else
    {
        //cout <<"Valid"<<endl;
        return true;
    }
}

int main()
{
    ifstream file("algo1-programming_prob-2sum.txt");
    string line;
    long long  a;

    count_i=0;
    while ( getline (file, line))
    {
        a = stoll(line);
        U_SET.insert (a);
        count_i++;
    }
    cout<<"Numbers Count: "<<count_i<<endl;

    /******** START: display Final *******************
    count_i=0;
    for (int  t = 10000; t >= -10'000 ; t--)
    {
        //if (t%500==0)
        //    cout <<t << endl;
        if (Two_sum (t) == true)
        {
           if (check_entry (a, b) == false )
              break;
           count_i++;
           cout<<"count: "<<count_i<<endl;
        }
    }
    cout << "Solution count: "<<count_i;
   /******** END: display Final *******************/

    /******** START: File Output *******************/
    ofstream outfile;
    outfile.open ("Result.txt");
    outfile<<"Two sum algorithm"<<endl;
    count_i=0;
    for (int  t = 10000; t >= -10'000 ; t--)
    {
        if (t%100==0)
            cout <<t << endl;

        if (Two_sum (t) == true)
        {
           if (check_entry (a, b) == false ){
              outfile<<"INVALID result"<<endl;
              break;
           }
           count_i++;
           outfile<<"count: "<<count_i<<endl;
           outfile<<t<<" ["<< a << ", "<<b<<" ]"<<endl;
        }
    }
    outfile<< "Total Solution(s): "<<count_i;
   /******** END: File Output *******************/

}
